# LoreBooks Inc#

LoreBooks is a platform that allows writers to turn their stories into virtual reality 3D e-books. We process story text into textures that are rendered onto 3D book models, creating an interactive and immersive reading experience. LoreBooks will award writers a percentage of advertising revenue based on the number of reads their content receives, similar to YouTube. Writers can purchase additional functionality, custom content, and merchandise. Our mission is to design the next generation of immersive storytelling through the use of virtual reality technology.

### What is this repository for? ###

* Hololens targeted development.
* Version 1.0
* [Link](https://www.ventureapp.com/company/lore-media/)

### How do I get set up? ###

* Clone lorebookenv repo at your local machine make sure you have Unity HoloLens 5.4.0f3-HTP (64-bit) editor to run the app. 
* To test the build on hololens emulator make sure you have Visual Studio update 3 installed along with hololens emulator on your machine.For more info checkout following [this link](https://developer.microsoft.com/en-us/windows/holographic/install_the_tools).


### Contribution guidelines ###

* Test Cases
* Code review


### Who do I talk to? ###

* Alan (Owner)
* Dinesh (Developer)