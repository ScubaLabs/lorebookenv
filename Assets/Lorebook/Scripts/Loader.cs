﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using TMPro;

public class Loader : MonoBehaviour
{
	public string filename = "examplebook.html";
	private delegate void Callback (string data);
	public string loadedText;

	void Start ()
    {
		LoadFile();	
	}

	public void LoadFile()
    {
		string filePath = Application.streamingAssetsPath + "/" + filename;
		#if UNITY_EDITOR
		filePath = "file://" +  filePath;
		#endif
		StartCoroutine(getFile(filePath, OnTextLoaded));
	}

	IEnumerator getFile(string assetPath, Callback callback)
    {
		WWW data = new WWW(assetPath);
		yield return data;
		if(string.IsNullOrEmpty(data.error)) {
			callback(data.text);
		}
	}

	public void OnTextLoaded(string text)
    {
		loadedText = text;
		gameObject.GetComponent<PagesController>().init(loadedText);
	}

}
