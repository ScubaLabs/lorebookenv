﻿using UnityEngine;
using System.Collections;

public class PageEvents : MonoBehaviour {

	public BookController bookController;

	void OnMouseUp () {

        Debug.Log("event clicked");
        if (gameObject.name.Equals("pagePrev")){
            Debug.Log("pagePrev clicked");
            bookController.prevPage();	
		} else if (gameObject.name.Equals("pageNext")){
            Debug.Log("pageNext clicked");
            bookController.nextPage();
		}
	}

	public void setColliderEnabled(bool val){
		GetComponent<BoxCollider>().enabled = val;
	}

}
