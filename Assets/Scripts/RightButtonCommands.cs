﻿using UnityEngine;

public class RightButtonCommands : MonoBehaviour
{
    public GameObject bookController;

    /// <summary>
    /// Called by GazeGestureManager when the user performs a Select gesture
    /// </summary>
    void OnSelect()
    {
        Debug.Log("next button OnSelect called");
        bookController.GetComponent<BookController>().nextPage();
    }
}