﻿using UnityEngine;
using UnityEngine.VR.WSA.Input;

public class GazeGestureManager : MonoBehaviour
{
    public static GazeGestureManager Instance { get; private set; }

    /// <summary>
    ///  Represents the hologram that is currently being gazed at.
    /// </summary>
    public GameObject FocusedObject { get; private set; }
    GestureRecognizer recognizer;

    /// <summary>
    ///  Use this for initialization
    /// </summary>
    void Start()
    {
        Instance = this;

        //Set up a GestureRecognizer to detect Select gestures.
        recognizer = new GestureRecognizer();
        recognizer.TappedEvent += (source, tapCount, ray) =>
        {
            Debug.Log("user taped the targeted game object");
            Debug.Log(FocusedObject.name);
            Debug.Log(source);
           
            // Send an OnSelect message to the focused object and its ancestors.
            if (FocusedObject.name == "Left" || FocusedObject.name == "ZoomIn" || FocusedObject.name == "ZoomOut" || FocusedObject.name == "Close" || FocusedObject.name == "Right"
                || FocusedObject.name == "Drag" || FocusedObject.name == "Movie" || FocusedObject.name == "Model" || true)
            {
                Debug.Log("Ready to call OnSelect method ");
                FocusedObject.SendMessageUpwards("OnSelect");

            } else if(SpatialMapping.Instance.DrawVisualMeshes)
            {
                 Debug.Log("Must drop the spatial now");
                 SpatialMapping.Instance.DrawVisualMeshes = false;
            }

           /* if (FocusedObject.name == "Drag")
            {
                Debug.Log("Ready to call OnSelect method ");
                SpatialMapping.Instance.DrawVisualMeshes = true;

            } */

        };
        recognizer.StartCapturingGestures();
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update()
    {
        // Figure out which hologram is focused this frame.
        GameObject oldFocusObject = FocusedObject;
        // Do a raycast into the world based on the user's
        // head position and orientation.
        var headPosition = Camera.main.transform.position;
        var gazeDirection = Camera.main.transform.forward;

        RaycastHit hitInfo;

        if (Physics.Raycast(headPosition, gazeDirection, out hitInfo)){
             // If the raycast hit a hologram, use that as the focused object.
             FocusedObject = hitInfo.collider.gameObject;
        } else {
            // If the raycast did not hit a hologram, clear the focused object.
            FocusedObject = null;
        }
        
        // If the focused object changed this frame,
        // start detecting fresh gestures again.
        if (FocusedObject != oldFocusObject)
        {
            Debug.Log("detected fresh gesture");
            recognizer.CancelGestures();
            recognizer.StartCapturingGestures();
            //if (oldFocusObject != null) oldFocusObject.SendMessageUpwards("OnHoverEnd");
            //if (FocusedObject != null) FocusedObject.SendMessageUpwards("OnHoverStart");
        }
    }
}