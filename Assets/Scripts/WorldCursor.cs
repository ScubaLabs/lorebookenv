﻿using UnityEngine;

public class WorldCursor : MonoBehaviour
{
    [SerializeField] Color highlightColor = Color.yellow;
    private Color defaultColor;
    private MeshRenderer meshRenderer;
    
    /// <summary>
    /// Use this for initialization
    /// </summary>
    void Start()
    {
        // Grab the mesh renderer that's on the same object as this script.
        meshRenderer = this.gameObject.GetComponentInChildren<MeshRenderer>();
        defaultColor = meshRenderer.material.color;
    }

    /// <summary>
    ///  Update is called once per frame
    /// </summary>
    void Update()
    {
        // Do a raycast into the world based on the user's
        // head position and orientation.
        var headPosition  = Camera.main.transform.position;
        var gazeDirection = Camera.main.transform.forward;
        RaycastHit hitInfo;

        if (Physics.Raycast(headPosition, gazeDirection, out hitInfo))
        {
            // If the raycast hit a hologram...
            // Display the cursor mesh.
            //Debug.Log("testing");
            //Debug.Log(hitInfo.collider.gameObject.name);
            if((hitInfo.collider.gameObject.name != "cover") && (hitInfo.collider.gameObject.name != "pageNext"))
                meshRenderer.enabled = true;
            else
                meshRenderer.enabled = false;
            // Move thecursor to the point where the raycast hit.
            this.transform.position = hitInfo.point;
             
            // Rotate the cursor to hug the surface of the hologram.
            this.transform.rotation = Quaternion.FromToRotation(Vector3.up, hitInfo.normal);
        }
        else
        {
            // If the raycast did not hit a hologram, hide the cursor mesh.
            meshRenderer.enabled = true;
            var f1 = gazeDirection * 3.0f;
            var finalPos = headPosition + f1;
            meshRenderer.enabled = true;
            this.transform.position = finalPos;
            this.transform.rotation = Quaternion.FromToRotation(Vector3.up, Vector3.forward);
        }
    }
}