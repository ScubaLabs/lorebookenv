﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]

public class PlayVideo : MonoBehaviour
{
    public MovieTexture movie;
    public AudioSource  audio;
    
    // Use this for initialization
    /* public void Start()
    {
        GetComponent<RawImage>().texture = movie as MovieTexture;
        audio = GetComponent<AudioSource>();
        audio.clip = movie.audioClip;
        movie.Play();
        audio.Play();
    } */
    
    /// <summary>
    /// Ativate movie texture
    /// </summary>
    public void StartMovieTexture()
    {
        GetComponent<RawImage>().texture = movie as MovieTexture;
        audio = GetComponent<AudioSource>();
        audio.clip = movie.audioClip;
        movie.Play();
        audio.Play();
    }

    /// <summary>
    /// Pause movie on monvie texture
    /// </summary>
    public void StopMovieTexture()
    {
        GetComponent<RawImage>().texture = movie as MovieTexture;
        audio = GetComponent<AudioSource>();
        audio.clip = movie.audioClip;
		movie.Pause();
        audio.Pause();
    }

}
