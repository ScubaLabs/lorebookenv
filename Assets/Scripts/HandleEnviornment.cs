﻿using UnityEngine;
using System.Collections;

public class HandleEnviornment : MonoBehaviour
{
	public GameObject bookController;
	public GameObject videoCanvas;
	public GameObject overlayModels;
	public GameObject bookCover;

	/* Action bar objects */
	public GameObject leftButton;
	public GameObject dragButton;
	public GameObject zoomInButton;
	public GameObject zoomOutButton;
	public GameObject closeButton;
	public GameObject rightButton;
	public GameObject videoButton;
//	public GameObject modelButton;
	public GameObject enviornmentButton;

	public bool showmodel = false;

	/// <summary>
	/// Called first time for initiating
	/// </summary>
	void Start()
	{
		overlayModels.SetActive(false);
		videoCanvas.SetActive(false);
	}

	/// <summary>
	/// Called when user performed an tap event on menu bar options
	/// </summary>
	void OnSelect()
	{
		Debug.Log("HandleModels OnSelect called");
		showmodel = !showmodel;
		if (showmodel)
		{
			overlayModels.SetActive(true);
			//  bookCover.transform.localRotation  = Quaternion.Euler(0,270,16);
			leftButton.SetActive(false);
			dragButton.SetActive(false);
			zoomInButton.SetActive(false);
			zoomOutButton.SetActive(false);
			closeButton.SetActive(false);
			rightButton.SetActive(false);
			videoButton.SetActive(false);
			//modelButton.transform.localRotation = Quaternion.Euler(-146, -0, 3);

		} else {

			overlayModels.SetActive(false);
			// bookCover.transform.localRotation  = Quaternion.Euler(0, 270, 75);
			leftButton.SetActive(true);
			dragButton.SetActive(true);
			zoomInButton.SetActive(true);
			zoomOutButton.SetActive(true);
			closeButton.SetActive(true);
			rightButton.SetActive(true);
			videoButton.SetActive(true);
			//modelButton.transform.localRotation = Quaternion.Euler(-67.6F, 9.27F, 178.48F);

		}  
	}
}
