﻿using UnityEngine;
using System.Collections;

public class HandleEnviornmentToggling : MonoBehaviour
{
    public GameObject bookController;
    public GameObject sphere360;
    public GameObject targetContainer;
    public GameObject spatialMaping;

    public bool envToggle = false;

    /// <summary>
    ///  Called first time for initiating
    /// </summary>   
    void Start()
    {

    }

    /// <summary>
    /// Called when user performed a tap event on env button
    /// </summary> 
    void OnSelect()
    {
        envToggle = !envToggle;
        if (envToggle)
        {
            sphere360.SetActive(true);
            targetContainer.SetActive(true);
            spatialMaping.SetActive(false);
        }
        else
        {
            sphere360.SetActive(false);
            targetContainer.SetActive(false);
            spatialMaping.SetActive(true);
        }
    }

}
